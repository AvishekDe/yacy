yacy
====

The new front end for SDSLabs Search powered by [yacy](http://yacy.net/en/index.html)


Setup :

* Install [yacy](http://yacy.net/en/index.html)
* Change config.ini to point to the correct yacy server
* Replace the paths from "/yacy/*" to the absolute path of your yacy directory in "views/layout.php"
