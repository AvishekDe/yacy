<!-- The partial view for the image results -->
<div id="imageResults">
	<?php  
	foreach($image_results['channels'][0]['items'] as $image){ ?>
	    <div class='result-elements'>
		<a title="<?php echo $image['title']?>" href="<?php echo $image['url']?>">
			<img class='result-elements' src="<?php echo $image['image']?>" />
		</a>
		</div>
	<?php  } ?>
</div>