<!-- The partial view for the landing page -->
<header class = 'search-wrapper' id = 'mainview'>
	<div class='form-wrapper'>
		<span id = "search-prompt">Search : </span>
		<form action = './index.php' method = 'get'>
			<input name='q' id = 'min-search-box'/>
			<button type = 'submit' id = 'main-search-button' class='btn btn-transparent'>
				<img src='./assets/images/search-button-main.png'>
			</button>
		</form>
	</div>
</header>