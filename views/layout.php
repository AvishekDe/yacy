<!-- Application's main view -->
<!DOCTYPE html>
<html>
	<head>
		<title>
			<?php if($query){echo $query." - ";}?>Search
		</title>
		<meta charset= 'utf-8'>
		<link rel="icon" type="image/png" href="../assets/images/search-button.png">
		<link rel="stylesheet" href="../assets/css/bootstrap.min.css">
		<link rel="stylesheet" href="../assets/css/main.css">
		<link href='http://fonts.googleapis.com/css?family=Open+Sans' rel='stylesheet' type='text/css'>
		<script src="https://cdn.sdslabs.co.in/cdnjs/ajax/libs/jquery/1.7.1/jquery.min.js"></script>
	    <script src="//sdslabs.co.in/home/assets/build/scripts/topbar.min.js"></script>
		<link rel="stylesheet" type="text/css" href="//sdslabs.co.in/home/assets/build/stylesheets/topbar.min.css">
		<script src="../assets/js/main.js"></script>
		<script src="../assets/js/jPaginate.js"></script>
		<script src="//sdslabs.co.in/api/public/api.js"></script>
		<script src="../assets/js/bootstrap.min.js"></script>
		<script src="../assets/js/masonry.min.js"></script>
    </head>


</html>