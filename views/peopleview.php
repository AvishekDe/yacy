<!-- The partial view for the people results -->

<div id="peopleResults">
	<?php 
	foreach($people_results['users'] as $people){ ?>
		<div class="result-elements">
			<a class="people-pic" href="https://accounts.sdslabs.co.in/<?php echo $people['username'];?>">
				<img class='thumbnailpic' src="https://accounts.sdslabs.co.in/image/<?php echo $people['uid']; ?>">
			</a>
			<a class='people-result-title' href="https://accounts.sdslabs.co.in/<?php echo $people['username']; ?>">
				<div class="ppname"><?php echo $people['name']; ?></div>
			</a>
			<div class="ppbranch">Department of <?php echo $people['branch']; ?></div>
			<div class="ppbryr ppsp">
				<?php
					echo $people['course']." (";
					switch ($people['year']) {
						case '1':
							echo "1st year";
							break;
						case '2':
							echo "2nd year";
							break;
						case '3':
							echo "3rd year";
							break;
						case '4':
							echo "4th year";
							break;
						case '5':
							echo "5th year";
							break;
						case '6':
							echo "6th year";
							break;
						
						default:
							echo "passout";
							break;
					}
					echo ")";
				?>
			</div>
		</div>
	<?php  } ?>	
</div> 