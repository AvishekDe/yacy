<!-- The partial view for the simple page results -->

<div id="pageResults">
	<?php  
	foreach($page_results->{'channels'}[0]->{'items'} as $page){ ?>
		<div class="result-elements">
			<h3>
				<a href="<?php echo $page->{'link'}?>">
					<?php echo $page->{'title'}?>
				</a>
			</h3>
			<a class='cache' href="<?php echo $conf['yacy']?>CacheResource_p.html?url=<?php echo $page->{'link'}?>">
				Cache 
			</a>
			<p class='page-link'>
				<?php echo substr($page->{'link'},0,100) ?>
			</p>
			<p class='page-desc'>
				<?php echo substr($page->{'description'},0,150) ?>
			</p>
			<p class='page-pub'>
				<?php echo substr($page->{'pubDate'},0,17) ?>
			</p>
		</div>
	<?php  } ?>	
</div>


