<!-- The partial view for the music results -->
<div id="musicResults" class="row">
	<?php $m = 0; ?>
	<?php foreach($music_results['tracks'] as $music){?>
		<div class='result-elements'>
			<a class="music-pic" href="https://sdslabs.co.in/muzi/#/track/<?php echo $music['id']?>/<?php echo preg_replace('/[^a-z0-9]/', '-', strtolower($music['title']))?>">
				<img class='thumbnailpic' src="https://cdn.sdslabs.co.in/music_pics/<?php echo $music['albumId']?>.jpg">
			</a>
			<a class='music-result-title' href="https://sdslabs.co.in/muzi/#/track/<?php echo $music['id']?>/<?php echo preg_replace('/[^a-z0-9]/', '-', strtolower($music['title']))?>">
				<div class="heightAdjuster"><?php echo $music['title'] ?></div>
			</a>
			<p class='music-result-artist'>
				<?php echo $music['artist']?>
			</p>
		</div>	
		<?php
		$m++;
		if($m==1000) break; 
		 } ?>
</div>