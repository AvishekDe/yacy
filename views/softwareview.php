<!-- The partial view for the software results -->
<div id="softwareResults">
	<?php  
	foreach($software_results as $software){ ?>
	    <div class='result-elements'>
			<div class="stitle">
			<img src='https://filepanda.sdslabs.co.in/public/soft_images/<?php echo $software['name'];?>.png'>
			<?php echo $software['title']; ?>
			</div><br>
			<a href="https://filepanda.sdslabs.co.in/software/<?php echo $software['name'];?>">Download</a><br>
			<div class="sdesc"><?php echo $software['description'];?></div>
			<div class="scat"> <b>Category: </b><?php echo $software['category'];?></div>
			<div class="snod"><b> Downloads: </b><?php echo $software['count'];?></div>
		</div>
	<?php  } ?>
</div>