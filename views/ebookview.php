<!-- The partial view for the ebook results -->
<div id="ebookResults">
	<?php  
	foreach($ebook_results['books'] as $ebook){ ?>
	    <div class='result-elements'>
			<a class="ebook-pic" href="https://echo.sdslabs.co.in/book/<?php echo $ebook['bookID']; ?>/<?php echo $ebook['title']; ?><?php echo preg_replace('/[^a-z0-9]/', '-', strtolower($ebook['title']))?>">
				<img class='thumbnailpic' src="https://echo.sdslabs.co.in/assets/thumbnails/thumb-<?php echo $ebook['bookID']?>.jpg">
			</a>
			<a class='ebook-result-title' href="https://echo.sdslabs.co.in/book/<?php echo $ebook['bookID']; ?>/<?php echo $ebook['title']; ?><?php echo preg_replace('/[^a-z0-9]/', '-', strtolower($ebook['title']))?>">
				<div class="ebtitle"><?php echo $ebook['title']; ?></div>
			</a>
			<div class="ebauthor"><?php echo $ebook['author']; ?></div>
			<a class="dl" href="https://echo.sdslabs.co.in/book/<?php echo $ebook['bookID'];?>/download">Download</a><br>
			<div class="ebisbn ebsp"><b>ISBN Number: </b><?php echo $ebook['isbn_10']; ?></div>
			<div class="ebrating ebsp"><b>Rating: </b><?php if($ebook['rating']==0) echo "Unrated";
			else echo $ebook['rating']; ?></div>
			<div class="ebformat ebsp"><b>Format: </b><?php echo $ebook['format']; ?></div>
		</div>
	<?php  } ?>
</div>