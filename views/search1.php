<!-- The partial view for the search page -->
<div id="leftbar">
   	<div id="searchlogo"><a class='logo' href=../index.php>Search</a>
   	</div>
   	
   	<div id="refinelogo">REFINE SEARCH
   	</div>

   	<div class='criteria' id="criteriasearch" > 
		<div class='criteria-list' id='pageButton' >
			Pages  <?php if ($p!=0){ echo '('. $p .')'; $flag=1;}?>
		</div>
        <div class='criteria-list' id='imageButton' >
		    Images <?php if ($i!=0){ echo '('. $i .')'; $flag=1;}?>
		</div>
		<div class='criteria-list' id='musicButton' >
			Music  <?php if ($m!=0){ echo '('. $m .')'; $flag=1;}?>
		</div>
		<div class='criteria-list' id='ebookButton' >
			Ebooks <?php if ($e!=0){ echo '('. $e .')'; $flag=1;}?>
		</div>  
		<div class='criteria-list' id='peopleButton' >
			People <?php if ($pe!=0){ echo '('. $pe .')'; $flag=1;}?>
		</div> 
		<div class='criteria-list' id='softwareButton' >
			Softwares <?php if($s!=0){echo '('. $s . ')'; $flag=1;}?>
		</div>
		<div class='criteria-list' id='otherButton' >
			Others
		</div>
	</div>
</div>

<div id="settings-navigator">
	
</div>


<header class = 'search-wrapper' id='searchview'>
	<div class='form-wrapper'>
		<form action = '../index.php' method = 'get'>
			<input name='q' id = 'search-box' class='form-control' />
			<button type = 'submit' id = 'search-button' class='btn btn-transparent'>
				<img src='../assets/images/search-icon.svg'>
			</button>
		</form>
		
	</div>
	<div class='numberofresults'>
	    <span id='number'>
		<?php if($p!=0)echo $p; ?>
		</span>
		<?php if($p!=0)echo " Pages, "; ?>
		<span id='number'>
		<?php if($i!=0)echo $i; ?>
		</span>
		<?php if($i!=0)echo " Images, "; ?>
		<span id='number'>
		<?php if($m!=0)echo $m; ?>
		</span>
		<?php if($m!=0)echo " Music results, "; ?>
		<span id="number">
		<?php if($e!=0)echo $e; ?>
		</span>
		<?php if($e!=0)echo " Ebooks,"; ?>
		<span id="number">
		<?php if($s!=0)echo $s; ?>
		</span>
		<?php if($s!=0)echo " Softwares,"; ?>
		<span id="number">
		<?php if($pe!=0)echo $pe; ?>
		</span>
		<?php if($pe!=0)echo " People "; ?>
		<?php if($flag!=0) echo " found.";?>
	</div>
</header>

