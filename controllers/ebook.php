<!-- eBook Search -->
<?php
    $url = $conf['ebook'].urlencode($query);

	$curlvar = curl_init($url);     
	curl_setopt($curlvar, CURLOPT_HEADER, 0);
	curl_setopt($curlvar, CURLOPT_RETURNTRANSFER, 1);
	$result = curl_exec($curlvar);
	$json = json_decode($result,'true');
	curl_close($curlvar);
	
	return $json;
?>