<?php
	
	class PageHandler{

		public function get($query){
			
				$conf = parse_ini_file("config.ini.sample");
				$url = $conf['yacy']."yacysearch.json?query=".urlencode($query)."&verify=ifexist&contentdom=text&startRecord=".$start."&maximumRecords=".$max."&urlmaskfilter=".$host;
				$curlvar = curl_init($url); 
			    
				curl_setopt($curlvar, CURLOPT_HEADER, 0);
				curl_setopt($curlvar, CURLOPT_RETURNTRANSFER, 1);
				$result = curl_exec($curlvar);
				$page_results = json_decode($result);
				curl_close($curlvar);

				$p=0;
				foreach($page_results->{'channels'}[0]->{'items'} as $page){
				$p++;
				}

				?>

			<script>
			var pagecount = <?php echo $p;?>;
			var highpriority = pagecount;
			</script>
			<div class="body-wrapper" id="important">

			<?php
				
				include "./views/layout.php";
				include "./views/search1.php";
			?>
				</div>
				<div class='results-wrapper' id='results'>
			<?php
				include "./views/pageview.php";
			?>
				</div>
				<div id = "noResults">
				<span id = "sad">:(</span> Nothing Found !
				</div>
			<?php	
				
		}
	}
?>