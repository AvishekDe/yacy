<?php
	
	class MusicHandler{
		public function get($query){
				$conf = parse_ini_file("config.ini.sample");
				$url = $conf['music']."?search=".$query; 
 				$curlvar = curl_init($url);     
				curl_setopt($curlvar, CURLOPT_HEADER, 0);
				curl_setopt($curlvar, CURLOPT_RETURNTRANSFER, 1);
				$result = curl_exec($curlvar);
				$music_results = json_decode($result,'true');
				curl_close($curlvar);

				$m=0;
				foreach($music_results['tracks'] as $music){
				$m++;
				if($m==1000) break;
				}
				?>

			<script>
			var musiccount = <?php echo $m;?>;
			var highpriority = musiccount;
			</script>
			<div class="body-wrapper" id="important">

			<?php
				
				include "./views/layout.php";
				include "./views/search1.php";
			?>
				</div>
				<div class='results-wrapper' id='results'>
			<?php
				include "./views/musicview.php";
			?>
				</div>
				<div id = "noResults">
				<span id = "sad">:(</span> Nothing Found !
				</div>
			<?php	
				
			
				
		}
	}
?>