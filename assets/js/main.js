$(document).ready(function(){
	$("input:text:visible:first").focus();
	// Show SDS topbar
	var topbar = new Topbar({ application: 'yacy' });
	// topbar.setCurrent('Search');
	var x = $.get_values;
	// Fills the input box value in second screen
	if (x["q"]){
		var search_q = x["q"].split("#");
		$("#search-box").val(search_q[0]);

		search_types = ["music", "page", "image", "software", "people", "ebook"];

		number_of_results = {};
		total = 0;
		search_types.forEach(function(type){
			number_of_results[type] = $("#" + type + "Results .result-elements").length;
			total += number_of_results[type];
		});

		// Checks if no results is to be shown
		if (total == 0){
			
			$("#noResults").show();
		}
		// Checks which tabs to show
		else{
			
			search_types.forEach(function(type){
				if (number_of_results[type] != 0){
					$("#" + type + "Button").css("display", "block");
				}
			});
		}
		

		// Selects a tab
		if (search_q.length == 1){
			selectTab("page"); 
		}
		else{	
			selectTab(search_q[1]);
		}

	}

	//Selecting default tab based on number of results
	if(musiccount == highpriority){
		selectTab("music");
		if(musiccount!=0){
			$("#musicResults").jPaginate({items:30});
		}
		else
		{
			$("#musicResults").html("<div id='oops'>Oops! Nothing Found Here!!</div><div id='danger'>Back off!! This might be a dangerous place!</div>");
		}
	}
	else if(imagecount == highpriority){
		selectTab("image");
		$("#imageResults").jPaginate({items:28});
	}
	else if(pagecount == highpriority){
		selectTab("page");
		$("#pageResults").jPaginate({items:10});
	}
	else if(softwarecount == highpriority){
		selectTab("software");
		$("#softwareResults").jPaginate({items:10});
	}
	else if(ebookcount == highpriority){
		selectTab("ebook");
		$("#ebookResults").jPaginate({items:10});
	}
	else if(peoplecount == highpriority){
		selectTab("people");
		$("#peopleResults").jPaginate({items:10});
	}

	// Onclick listener on each tab button
	$(".criteria-list").click(function(){
		var btn = $(this).attr("id");
		var splitName=btn.split("B")[0];
		selectTab(splitName);
		var ndispvar; //to store number of results of chosen tab
		var paginationVar="#" + splitName + "Results";
		// Generating number of results of chosen tab

		switch(splitName){
			case "page":ndispvar=pagecount;
			break;
			case "image":ndispvar=imagecount;
			break;
			case "music":ndispvar=musiccount;
			break;
			case "ebook":ndispvar=ebookcount;
			break;
			case "people":ndispvar=peoplecount;
			break;
			case "software":ndispvar=softwarecount; 
		}
		
		//Display results div
		if(ndispvar!=0){
			if(splitName=="page"){
				$(paginationVar).jPaginate({items:10});
			}
			if(splitName=="music"){
				$(paginationVar).jPaginate({items:30});
			}
			if(splitName=="image"){
				$(paginationVar).jPaginate({items:28});
			}
			if(splitName=="software"){
				$(paginationVar).jPaginate({items:10});
			}
			if(splitName=="ebook"){
				$(paginationVar).jPaginate({items:10});
			}
			if(splitName=="people"){
				$(paginationVar).jPaginate({items:10});
			}
		}

		//else display no_results div
		else
		{
			$("#" + splitName + "Results").html("<div id='oops'>Oops! Nothing Found Here!!</div><div id='danger'>Back off!! This might be a dangerous place!</div>");
		}
	});
});

//-------------------------- FUNCTIONS

// Function to get the GET parameters
$.get_values  = (function(string) {
	if (string == "") return {};
	var out = {};
	for (var i = 0; i <string.length; ++i){
		var str = string[i].split('=');
		if (str.length != 2) continue;
		out[str[0]] = decodeURIComponent(str[1].replace(/\+/g, " "));
	}
	return out;
})(window.location.search.substr(1).split('&'));

// Function to select a tab
function selectTab(type){
	$("#" + type + "Button").siblings().removeClass("page");
	$("#" + type + "Button").addClass("page");

	$("#" + type + "Results").siblings().css("display", "none");
	$("#" + type + "Results").css("display", "block");
	$("#topbar").css("display", "block");
	
}

$(document).ready(function(){
	$(".pagination").css("display" , "block");
})