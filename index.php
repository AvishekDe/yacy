<?php
 include 'toro.php';



if(isset($_GET['q'])) {
   $clean_url = './' . $_GET['q'];
   // code to create a clean url. 
   // After that $clean_url will contain the new redirect url 
   header("Location: $clean_url");


}

ToroHook::add("404",  function() {
    header('HTTP/1.1 404 Not Found');
    echo 'Error 404 Page Not Found';
    exit;
});

include 'handlers/HomeHandler.php';
include 'handlers/EverythingHandler.php';
include 'handlers/PageHandler.php';
include 'handlers/MusicHandler.php';
include 'handlers/ImagesHandler.php';
include 'handlers/EbooksHandler.php';
include 'handlers/PeopleHandler.php';
include 'handlers/SoftwareHandler.php';

Toro::serve(array(
  '/' => "HomeHandler",
  '/([a-zA-Z0-9- ]+)' => "EverythingHandler",
  '/page/([a-zA-Z0-9- ]+)' => "PageHandler",
  '/music/([a-zA-Z0-9- ]+)' => "MusicHandler",
  '/images/([a-zA-Z0-9- ]+)' => "ImagesHandler",
  '/ebooks/([a-zA-Z0-9- ]+)' => "EbooksHandler",
  '/people/([a-zA-Z0-9- ]+)' => "PeopleHandler",
  '/software/([a-zA-Z0-9- ]+)' => "SoftwareHandler"
  
  ));

?>
